const jwt = require('jsonwebtoken');

function PaaswordAuthenticate(req, res, next) {
	try {
		const token = req.header('x-auth-token');
		if (!token) return res.status(401).send({ ErrorType: 'MISSING_ELEMENT', ErrorMessage: 'x-auth-token' });
        
		const appPrivateKey = process.env.PAASWORD_APP_PRIVATE_KEY;
		if (!appPrivateKey) return res.status(401).send({ ErrorType: 'MISSING_ELEMENT', ErrorMessage: 'PAASWORD_APP_PRIVATE_KEY' });
        
		const user = jwt.verify(token, appPrivateKey);
		if (user && user.AutoLogout.IsEnabled) {
			const loginTime = user.iat * 1000;
			const hoursSinceLogin = Math.round((Date.now() - loginTime)/3600000);
			if (hoursSinceLogin > user.AutoLogout.LogoutEveryXHours) {
				return res.status(401).send({ ErrorType: 'SESSION_EXPIRED', ErrorMessage: '' });
			}
		}
		req.user = user;

		next();

	} catch (ex) {
		return res.status(401).send({ ErrorType: 'INTERNAL_ERROR', ErrorMessage: ex.message });
	}
}
module.exports = PaaswordAuthenticate;