# Paasword Node Middleware

[Paasword](https://www.paas-word.com) is an online authentication and user management service.
This Node.js middleware library by [Paasword](https://www.paas-word.com) enables website owners with a node.js backend to restrict their endpoints to authenticated users only and retrieve user data. 

## Usage

1. Create a free account at [Paasword](https://www.paas-word.com) website.
2. Recieve a login, sign-up, account and forgot-password pages for your website based on the user attributes you set up.
3. Set the callback pages on your website where users will be redirected after they sign-up and log in. 
4. Once a user is redirected to your website with a token, send this token to your backend in the "x-auth-token" header.

## Installation

`npm install paasword-node --save`

## Set Private Key as Environment Variable
Create an app on [Paasword](https://www.paas-word.com) and then set its Private Key as an environment variable on your server.

`export PAASWORD_APP_PRIVATE_KEY=93f56f52-957d-4953-93a6-c5492e79778b`

## Import
in your routes file:

```javascript
const PaaswordAuthenticate = require('paasword-node');

```

## Gaurd all endpoints
Guard all endpoints against unauthorized users with one line of code.

```javascript
const PaaswordAuthenticate = require('paasword-node');

app.use(PaaswordAuthenticate);

app.get('/api/private', async(req, res) => {
    return PrivateManager.getPrivateInfo(req, res);
});

app.post('/api/private', async(req, res) => {
    return PrivateManager.setPrivateInfo(req, res);
});
```

## Gaurd spesific routes against unauthenticated users
You should place these gaurds on routes dedicated to logged-in users. 

```javascript
const PaaswordAuthenticate = require('paasword-node');

app.get('/api/private', PaaswordAuthenticate, async(req, res) => {
    return PrivateManager.getPrivateInfo();
});
```

## Retrieve user information

```javascript
const PaaswordAuthenticate = require('paasword-node');

app.get('/api/private', PaaswordAuthenticate, async(req, res) => {
    console.log(req.user);
    return PrivateManager.getPrivateInfo(req, res);
});
```

